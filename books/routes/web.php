<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () { return view('welcome'); });

Route::get('/',                         'HomeController@index'      )->name('home');
Route::get('/book/{archive_id}/{fileName}',     function ($archive_id,$fileName){ return (new App\Http\Controllers\HomeController)->book_page($archive_id,$fileName); })->name('getBook');
Route::get('/download/{archive_id}/{fileName}', function ($archive_id,$fileName){ return (new App\Http\Controllers\HomeController)->download($archive_id,$fileName); })->name('download');
Route::match(['get', 'post'],'/search', 'HomeController@search'     )->name('search');
Auth::routes();


Route::middleware('auth')->group(function(){

    Route::get('/profile', 'ProfileController@index')->name('profile');

    Route::namespace('Admin')->group(function () {

        Route::get('/admin','AdminController@index')->name('adminHome');


        Route::match(['get', 'post'],'/admin/statistic',    'AdminController@index'         )->name('adminStatistic');
        Route::match(['get', 'post'],'/admin/update_books', 'AdminController@update_books'  )->name('adminUpdateBooks');
        Route::match(['get', 'post'],'/admin/update_zip',   'AdminController@update_zip'    )->name('adminUpdateZip');
        Route::match(['get', 'post'],'/admin/users',        'AdminController@users'         )->name('adminUsers');

        Route::get('/admin/update_books/findallfiles',      'AdminController@findAllFiles'  )->name('adminFindAllFiles');
        Route::get('/admin/update_books/parseonearhive',      'AdminController@parseOneArhive'  )->name('adminParseOneArchive');
        Route::get('/admin/update_books/clearallbooks',      'AdminController@clearAllBooks'  )->name('adminClearAllBooks');
        Route::get('/admin/update_books/createindexes',      'AdminController@create_indexes'  )->name('adminCreateIndexes');
    });


});



