

/*
window.addEventListener("load", function(event) { });
*/
document.addEventListener("DOMContentLoaded", function(){


    /* TABS */

    let tabs = document.querySelectorAll('.tabs');

    tabsInit();
    function tabsInit() {
        Array.prototype.forEach.call(tabs, function (tab) {
            let tabsLinks = tab.querySelectorAll('.tabs-header > a');
            let contents = tab.querySelectorAll('.tabs-content');
            Array.prototype.forEach.call(tabsLinks, function (link, linkIndex) {
                link.onclick = function () {
                    Array.prototype.forEach.call(contents, function (c, ci) {
                        if (ci === linkIndex) {
                            tabsLinks[ci].classList.add("active");
                            c.classList.add("active");
                            window.location.hash = link.getAttribute('data-tab');
                        } else {
                            tabsLinks[ci].classList.remove("active");
                            c.classList.remove("active");
                        }
                    });
                    return false;
                }
            });
        });
    }

    function tabsUnselect(tab){
        let tabsLinks = tab.querySelectorAll('.tabs-header > a');
        let contents = tab.querySelectorAll('.tabs-content');
        Array.prototype.forEach.call(tabsLinks, function (link, linkIndex) {
            link.classList.remove('active');
        });
        Array.prototype.forEach.call(contents, function (cont, ci) {
            cont.classList.remove('active');
        });
    }

    function tabsSelect(tabHash){
        Array.prototype.forEach.call(tabs, function (tab) {
            let tabsLink = tab.querySelectorAll('.tabs-header > a[data-tab="'+ tabHash +'"]');
            console.log('tabsLinks('+tabHash+')',tabsLink);
            if(typeof tabsLink[0] !== "undefined"){

                tabsUnselect(tab);
                tabsLink[0].classList.add('active');
                let cont = tab.querySelectorAll('.tabs-content[data-tab="'+ tabHash +'"]');
                if(typeof cont[0] !== "undefined") {
                    cont[0].classList.add('active');
                }
            } else console.log('Tabs not found');
        });
    }

    tabsOnLoad();
    function tabsOnLoad(){
        tabsSelect(window.location.hash.substr(1));
    }




});
