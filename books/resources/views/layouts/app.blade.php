<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Books') . ($page['title']??false?' - '.$page['title']:'') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,400,800&display=swap&subset=cyrillic,cyrillic-ext" rel="stylesheet">

    <link href="{{ asset('css/buttons-min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/forms-min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/menus-min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/rushu.css') }}" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        #app {
            min-height: 100vh;
            position: relative;
            padding-bottom: 3em;
        }
        footer {
            position: fixed;
            bottom: 0;
            width: 100%;
            text-align: center;
            font-size: 70%;
            line-height: 2em;
            background: rgb(255,255,255);
            background: linear-gradient(90deg, rgba(255,255,255,0) 0%, rgba(255,255,255,0.8) 40%, rgba(255,255,255,0.9) 50%, rgba(255,255,255,0.8) 60%, rgba(255,255,255,0) 100%);
        }
    </style>
</head>

<body>
    <div id="app">

        @extends('components.nav-top')



        @yield('content')

        <footer>
            <a href="mailto:warui@bk.ru">WARUi</a> @ {{ date('d-m-Y') }}
        </footer>

    </div>

    <script src="{{ asset('js/rushu.js') }}"></script>
</body>
</html>
