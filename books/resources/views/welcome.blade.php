@extends('layouts.app')

@section('content')

    <style>
        .first-section form fieldset input,
        .first-section form fieldset button {
            height: inherit;
            /*padding: .5em 1em;*/
            font-size: 1em;
        }

        .table a  {
            color: inherit;
            text-decoration: none;
        }


    </style>

    <main>

        <div class="first-section @if($search) min @endif">
            <div class="wrap">
                <h1 class="title mt-3 mb-0">Find this book!</h1>
                <div class="center">
                    <form method="POST" action="{{ route('search') }}" class="pure-form">
                        <fieldset>
                        @csrf

                            <input type="text" class=" @error('search') is-invalid @enderror" name="search" value="{{ old('search')??$search }}" required placeholder="Search" autocomplete="search" >
                            <button type="submit" class="pure-button pure-button-primary">Search</button>

                            @error('search')
                            <span class="pure-form-message" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror



                        </fieldset>

                    </form>
                </div>
            </div>
        </div>

        <div class="container">

            @if($search)

                <div class="center">
                    <h2>Search:</h2>
                </div>

                @if($results)
                    <?php
                    $authors = [];
                    foreach($results['authors'] as $author){ $authors[] = $author->authors; }
                    ?>
                    @if(count($authors)>0)
                        <div class="center">
                            <h4 class="mb-1">Similar authors</h4>
                            <div clas="similar-authors">{{ implode(', ',$authors) }}</div>
                        </div>
                    @endif

                    <h4 class="mb-1">Books:</h4>
                    <div class="table results-table">
                        <div class="row header">
                            <div class="col-3 col-sm-12">Authors</div>
                            <div class="col-3 col-sm-6">Title</div>
                            <div class="col-3 col-sm-6">Sequence</div>
                            <div class="col-2 col-sm-6">Lang</div>
                            <div class="col-1 col-sm-6">Date</div>
                        </div>

                    @foreach($results['books'] as $res)
                        <?php
                            $lang_src = explode(';',$res->lang_src);
                            $lang_src = implode('<br>',$lang_src);
                        ?>
                        <a class="row {{ $loop->even?'even':'odd' }}" href="{{ route('getBook', [$res->archive_id, $res->file_name]) }}">
                            <div class="col-3 col-sm-12 pt-sm-2 pb-sm-1">{{ $res->authors }}</div>
                            <div class="col-3 col-sm-6 bold-sm pl-sm-3">{{ $res->title }}</div>
                            <div class="col-3 col-sm-6">{{ $res->sequence_name }}</div>
                            <div class="col-2 col-sm-6 pl-sm-3">{{ $res->lang }} {!! strlen($res->lang_src)>0 ? '<span class="font-small"> &lArr; '.$lang_src.'</span>' : '' !!}</div>
                            <div class="col-1 col-sm-6 font-small">{{ $res->date }}</div>
                        </a>
                    @endforeach
                    </div>

                @else
                    <h5>Sorry! Nothing found</h5>
                @endif

            @else
                <div class="center">
                <h2>Statistic</h2>
                <p>Books in library: {{ $statistic['books']??'' }}</p>
                <p>Last updated: {{ $statistic['date_lastUpdate']??'' }}</p>
                </div>
            @endif


        </div>

    </main>
@endsection
