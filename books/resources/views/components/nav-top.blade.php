
<nav class="top">
    <div class="container">

        <div>
            <div class="pure-menu pure-menu-horizontal" >
                <a href="/" class="pure-menu-heading pure-menu-link app-name">{{ config('app.name') }}</a>
                <ul class="pure-menu-list">
                    @if(Auth::check())
                    <li class="pure-menu-item"><a href="{{ route('profile') }}" class="pure-menu-link">{{ __('Profile') }}</a></li>

                        @if(Auth::user()->isAdmin())
                            <li class="pure-menu-item"><a href="{{ route('adminUpdateBooks') }}" class="pure-menu-link">{{ __('Administration') }}</a></li>
                            <?php /*
                            <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
                                <a href="{{ route('adminUpdateBooks') }}" id="menuLink1" class="pure-menu-link">{{ __('Administration') }}</a>
                                <ul class="pure-menu-children">
                                    <li class="pure-menu-item"><a href="{{ route('adminStatistic') }}"   class="pure-menu-link">{{ __('Statistics') }}</a></li>
                                    <li class="pure-menu-item"><a href="{{ route('adminUpdateBooks') }}" class="pure-menu-link">{{ __('Update all books') }}</a></li>
                                    <li class="pure-menu-item"><a href="{{ route('adminUpdateZip') }}"   class="pure-menu-link">{{ __('Update zip archive') }}</a></li>
                                    <li class="pure-menu-item"><a href="{{ route('adminUsers') }}"       class="pure-menu-link">{{ __('Users') }}</a></li>
                                </ul>
                            </li>
                            */?>
                        @endif
                    @endif
                </ul>
            </div>
        </div>

        <div>
            <div class="pure-menu pure-menu-horizontal">
                <ul class="pure-menu-list">
                    @guest
                        <li class="pure-menu-item"><a class="pure-menu-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        @if (Route::has('register'))
                            <li class="pure-menu-item"><a class="pure-menu-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @endif
                    @else

                        <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
                            <a href="{{ route('profile') }}" id="menuLink1" class="pure-menu-link">{{ Auth::user()->name }}</a>
                            <ul class="pure-menu-children">
                                <li class="pure-menu-item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="pure-menu-link">{{ __('Logout') }}</a></li>
                            </ul>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
                </ul>
            </div>
        </div>

    </div>
</nav>
