@extends('layouts.app')

@section('content')

    <style>
        .first-section {
            background-image: url("/imgs/admin_02.jpeg");
        }
    </style>


    <main>

        <div class="first-section min">
            <h1 class="title">
                {{ $page['title'] }}
            </h1>
        </div>

        <div class="container">

            @include($page['view'])

        </div>

    </main>
@endsection
