

<h3>Update one archive</h3>

<form action="{{ route('adminUpdateZip') }}" method="POST" class="pure-form pure-form-stacked">
    @csrf

    <fieldset>
        <div class="fieldset">
            <label for="input_zip">{{ __('Zip archive name') }}</label>
            <input id="input_zip" type="text" class="form-control @error('zip') is-invalid @enderror" name="zip" value="{{ old('zip') }}" required placeholder="fb2-999999-999999.zip">
            @error('zip')
            <span class="pure-form-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <button type="submit" class="pure-button pure-button-primary" onclick="confirm('{{ __('Confirm operation') }}')">{{ __('Do') }}</button>
    </fieldset>
</form>
