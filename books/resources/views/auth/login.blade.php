@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row align-content-center">
            <div class="col-5 col-sm-10 pt-5">


                <form method="POST" action="{{ route('login') }}" class="pure-form pure-form-stacked">
                    <fieldset>

                        @csrf

                        <legend><h2 class="card-header">{{ __('Login') }}</h2></legend>


                        <div class="fieldset">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" >
                            @error('email')
                            <span class="pure-form-message" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="fieldset">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
                            @error('password')
                            <span class="pure-form-message" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>


                        <button type="submit" class="pure-button pure-button-primary">{{ __('Login') }}</button>


                        @if (Route::has('password.request'))
                            <a class="button" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif

                    </fieldset>
                </form>


            </div>
        </div>
    </div>

@endsection
