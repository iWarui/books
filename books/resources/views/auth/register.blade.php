@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-content-center">
        <div class="col-5 col-sm-10 pt-5">

            <form method="POST" action="{{ route('register') }}" class="pure-form pure-form-stacked">
                <fieldset>

                    @csrf

                    <legend><h2 class="card-header">{{ __('Register') }}</h2></legend>

                    <div class="fieldset">
                        <label for="name">{{ __('Name') }}</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="pure-form-message" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="fieldset">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" >
                        @error('email')
                        <span class="pure-form-message" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="fieldset">
                        <label for="password">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
                        @error('password')
                        <span class="pure-form-message" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="fieldset">
                        <label for="password_confirmation">{{ __('Confirm Password') }}</label>
                        <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required>
                        @error('password_confirmation')
                        <span class="pure-form-message" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="pure-button pure-button-primary">{{ __('Register') }}</button>


                </fieldset>
            </form>


        </div>
    </div>
</div>
@endsection
