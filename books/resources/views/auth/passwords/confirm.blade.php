@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row align-content-center">
            <div class="col-5 col-sm-10 pt-5">


                <form method="POST" action="{{ route('password.confirm') }}" class="pure-form pure-form-stacked">
                    <fieldset>

                        @csrf

                        <legend><h2 class="card-header">{{ __('Confirm Password') }}</h2></legend>


                        <div class="fieldset">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
                            @error('password')
                            <span class="pure-form-message" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <button type="submit" class="pure-button pure-button-primary">{{ __('Confirm Password') }}</button>

                        @if (Route::has('password.request'))
                            <a class="button" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif


                    </fieldset>
                </form>


            </div>
        </div>
    </div>


@endsection
