@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row align-content-center">
            <div class="col-5 col-sm-10 pt-5">


                <form method="POST" action="{{ route('password.email') }}" class="pure-form pure-form-stacked">
                    <fieldset>

                        @csrf

                        <legend><h2 class="card-header">{{ __('Reset Password') }}</h2></legend>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif




                        <div class="fieldset">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" >
                            @error('email')
                            <span class="pure-form-message" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <button type="submit" class="pure-button pure-button-primary">{{ __('Send Password Reset Link') }}</button>

                    </fieldset>
                </form>

            </div>
        </div>
    </div>
@endsection
