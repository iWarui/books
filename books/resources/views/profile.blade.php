@extends('layouts.app')

@section('content')

    <style>
        .first-section {
            position: relative;
            width: 100%; height: 8em;
            background-image: url("/imgs/books_02.jpeg");
            background-position: center;
            background-size: cover;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }
        .first-section .title {
            text-align: center;
            font-size: 500%;
            font-weight: 200;
            color: lightgrey;
        }
        .first-section:before {
            content: " ";
            position: absolute;
            bottom: 0; left: 0; right: 0;
            height: 5em;
            background: linear-gradient(0deg, rgba(255,255,255,.5) 0, rgba(255,255,255,0) 100%);
        }
    </style>

    <main>

        <div class="first-section">
            <h1 class="title">
                {{ $page['title'] }}
            </h1>
        </div>

        <div class="container">

            <div class="center">
                <h2>Profile</h2>
                <p>Coming soon...</p>
            </div>

        </div>

    </main>
@endsection
