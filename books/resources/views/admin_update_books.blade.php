<h3>Update all books</h3>


<p>The process will scan all archives and save parsed fb2-data to the database</p>

<div class="pure-form">
    <div class="fieldset">
        <label for="input_threads">Threads:</label>
        <input id="input_threads" type="text" value="3" title="count threads of process" style="width: 5em">

        <label for="input_pause" class="pl-3">Pause:</label>
        <input id="input_pause" type="text" value="1" title="pause between threads" style="width: 5em">
    </div>
</div>

<p>
    <a id="btn_update" class="pure-button pure-button-primary" onclick="do_update()">{{ __('Update') }}</a>
    <a id="btn_stop" class="pure-button pure-button-primary hidden" onclick="do_stop()">{{ __('Stop') }}</a>
    <a id="btn_indexes" class="pure-button pure-button-primary" onclick="do_indexes()" title="Indexes are needed for quick search">{{ __('Create indexes') }}</a>
</p>

<div id="log" class="hidden">
    <b>Count: </b><span class="count"></span>
    <b>Errors: </b><span class="errors"></span>
    <b>Time: </b><span class="time"></span>
</div>



<div class="labels">
    <div><span class="in_db"></span> - archive in DB </div>
    <div><span class="in_disk"></span> - archive in disk </div>
</div>
<div id="archives">

</div>


<div style="display: none" id="archive_blank">
    <div class="archive">
        <div class="name">lib.rus.ec/fb2-000065-572310_lost.zip</div>
        <div class="info">
            <div class="counts">
                <div><span>Count:</span>  <b>300</b></div>
                <div><span>Readed:</span> <b>267</b></div>
                <div><span>Errors:</span> <b>53</b></div>
            </div>
            <div class="time">Time: <b>0.154c</b></div>
        </div>
    </div>
</div>


<script>

    let log = document.getElementById('log');
    let list = document.getElementById('archives');
    let listArchives = [];
    let listDomArchives = [];
    let update_process = false; // флаг процесса обновления
    let threads = 4;                // counts
    let pause_before_new_task = 1;  // seconds
    let countFB2 = 0;
    let countErrorsFB2 = 0;
    let countTime = 0;
    let needRebuildArchives = true;
    let timer;


    Number.prototype.toHHMMSS = function () {
        var sec_num = parseInt(this, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds;
    };


    function updateMainStatus() {
        log.classList.remove('hidden');
        log.querySelector('.count').innerHTML = countFB2;
        log.querySelector('.errors').innerHTML = countErrorsFB2;
        log.querySelector('.time').innerHTML = countTime.toHHMMSS(); //hours + 'h ' + minutes + 'm ' + seconds + 's';
    }


    function create_arch(arch) {
        let e = document.createElement('div');
        e.className = 'archive';
        let name = document.createElement('div');
        name.className = 'name';
        name.innerHTML = arch.name;
        e.appendChild( name );
        let info = document.createElement('div'); info.className = 'info';
        e.appendChild( info );

        if((typeof arch.file !== 'undefined') && arch.file) {
            let in_disk = document.createElement('div');
            in_disk.className = 'status_ico in_disk';
            e.appendChild( in_disk );
        }

        if((typeof arch.bd_data !== 'undefined') && arch.bd_data) {
            let in_db = document.createElement('div');
            in_db.className = 'status_ico in_db';
            in_db.setAttribute('title', 'created_at: ' + arch.bd_data.created_at);
            e.appendChild( in_db );
        }
        return e;
    }

    function build_list(){
        let blankArch = document.getElementById('archive_blank');
        list.innerHTML = "";
        for (let i=0;i<listArchives.length;i++){
            list.appendChild( create_arch( listArchives[i] ) );
        }
        listDomArchives = list.querySelectorAll('.archive');
    }


    function findAllFiles(){
        if(!needRebuildArchives) return false;

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            //console.log("Request status:",this.readyState );
            if (this.readyState === 4 && this.status === 200) {
                let data = JSON.parse( xhttp.responseText );
                if(typeof data.archives !== 'undefined' ) {
                    listArchives = data.archives;
                    build_list();
                    needRebuildArchives = false;
                } else if(typeof data.msg !== 'undefined') {
                    list.innerHTML = '<p class="' + data.status + '">' + data.msg + '</p>';
                }
            }
        };
        xhttp.open("GET", "{{ route('adminFindAllFiles') }}", true);
        xhttp.send();
    }





    function parse_archive(){
        if(!update_process) return false;
        // find next archive to parse
        let arch;
        let index = false;
        for (let i=0;i<listArchives.length;i++){
            if( typeof listArchives[i].status === "undefined" ){
                arch = listArchives[i];
                listArchives[i].status = 1; // statuses: 1-parsing; 2-parsed; 3-error
                index = i;
                break;
            }
        }
        if(index !== false) {

            if(typeof listDomArchives[index] !== "undefined") {
                let domArchive = listDomArchives[index];

                domArchive.querySelector('.info').innerHTML = 'Parsing...';
                domArchive.classList.add('inprogress');

                let req = new XMLHttpRequest();

                req.onerror = function(){
                    domArchive.querySelector('.info').innerHTML = '<div class="error">ERROR 500</div>';
                    domArchive.classList.remove('inprogress');
                    domArchive.classList.add('error');
                };

                req.onreadystatechange = function () {
                    if (this.readyState === 500) {
                        domArchive.querySelector('.info').innerHTML = '<div class="error">ERROR 500</div>';
                        domArchive.classList.remove('inprogress');
                        domArchive.classList.add('error');
                    }

                    if (this.readyState === 4 && this.status === 200) {
                        let data = JSON.parse(req.responseText);
                        if (typeof data.status !== 'undefined') {

                            if (data.status === 'error') {
                                domArchive.classList.remove('inprogress');
                                domArchive.classList.add('haserror');
                            } else {
                                domArchive.classList.remove('inprogress');
                                domArchive.classList.add('success');
                            }

                            if(typeof data.data !== 'undefined') {
                                //INFO: data.data = [time, fb2_readed, fb2_read_errors, fb2_deleted, archive_is_deleted, all_count]

                                countFB2 = data.data.all_count;
                                countErrorsFB2 += data.data.fb2_read_errors;

                                if ((typeof data.data.archive_is_deleted !== 'undefined') && data.data.archive_is_deleted) {
                                    domArchive.querySelector('.info').innerHTML = '<div class="' + data.status + '">Archive is deleted. Time: ' + data.data.time.toHHMMSS() + '</div>';
                                } else {
                                    let text = '<b>Readed:</b> ' + data.data.fb2_readed;
                                    text += ' <b>Errors:</b> ' + data.data.fb2_read_errors;
                                    if (data.data.fb2_deleted > 0) text += ' <b>Deleted:</b> ' + data.data.fb2_deleted;
                                    text += ' <b>Time:</b> ' + data.data.time.toHHMMSS();
                                    domArchive.querySelector('.info').innerHTML = text;
                                }
                            }

                            updateMainStatus();
                            setTimeout( function () {
                                if( !parse_archive() ) { // do next
                                    // no next arch
                                    document.getElementById('btn_stop').classList.add('hidden');
                                    let inprogress = list.querySelectorAll('.inprogress');
                                    if( (typeof inprogress !== 'undefined') && (inprogress.length >0) ) {
                                        // some process in progress
                                    } else {
                                        document.getElementById('btn_indexes').classList.remove('hidden');
                                    }
                                }
                            }, (pause_before_new_task * 1000) );
                        } else {
                            domArchive.querySelector('.info').innerHTML = '<span class="error">Error request parsing</span>';
                        }
                    }
                };
                req.open("GET", "{{ route('adminParseOneArchive') }}?file="+arch.name, true);
                req.send();

            }
        } else {
            update_process = false;
            return false;
        }
    }



    function updating(){
        if(update_process) {

            timer = window.setInterval(function(){
                countTime++;
                updateMainStatus();
                let in_process = list.querySelectorAll('.inprogress');
                if(in_process && (in_process.length === 0)) {

                    document.getElementById('btn_update').classList.remove('hidden');
                    document.getElementById('btn_indexes').classList.remove('hidden');
                    document.getElementById('btn_stop').classList.add('hidden');

                    clearInterval(timer);
                } else {
                    document.getElementById('btn_update').classList.add('hidden');
                    document.getElementById('btn_indexes').classList.add('hidden');
                    document.getElementById('btn_stop').classList.remove('hidden');
                }


            },1000);

            let countThreads = 0;
            let threadsInterval = window.setInterval(function(){
                countThreads++;
                if(countThreads >= threads) clearTimeout( threadsInterval );
                parse_archive();
            },100);
        }

    }

    function create_indexes() {
        let req = new XMLHttpRequest();
        req.onerror = function(){
            alert('Error clear books table!');
        };
        req.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let data = JSON.parse(req.responseText);
                log.classList.remove('hidden');
                let row = document.createElement('div');
                row.innerHTML = data.msg;
                log.appendChild(row);

            }
        };
        req.open("GET", "{{ route('adminCreateIndexes') }}", true);
        req.send();
    }



    //// BTNs

    function do_update() {
        if( confirm('{{ __('Confirm operation') }}') ) {

            let btn_stop = document.getElementById('btn_stop'); btn_stop.classList.remove('pure-button-disabled'); btn_stop.innerHTML = 'Stop';

            update_process = true;
            countFB2 = 0;
            countErrorsFB2 = 0;
            countTime = 0;
            let input_threads = document.getElementById('input_threads');
            threads = parseInt( input_threads.value );
            let input_pause = document.getElementById('input_pause');
            pause_before_new_task = parseInt( input_pause.value );

            updating();
        }
    }


    function do_stop() {
        if( confirm('{{ __('Confirm operation') }}') ) {
            clearTimeout(timer);
            let btn_stop = document.getElementById('btn_stop'); btn_stop.classList.add('pure-button-disabled'); btn_stop.innerHTML = 'Stopping...';

            update_process = false;
        }
    }

    function do_indexes(){
        if( confirm('{{ __('Confirm operation') }}') ) {
            //TODO: do request to create indexes create_indexes()
            create_indexes();
        }
    }

    window.onload = function () {
        findAllFiles();

    }
</script>
