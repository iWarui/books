@extends('layouts.app')

@section('content')

    <style>
        .first-section form fieldset input,
        .first-section form fieldset button {
            height: inherit;
            font-size: 1em;
        }

        label {
            display: inline-block;
            min-width: 7em;
            font-weight: 600;
        }
    </style>

    <main>

        <div class="first-section min">
            <div class="wrap">
                <h1 class="title mt-3 mb-0">Find this book!</h1>
                <div class="center">
                    <form method="POST" action="{{ route('search') }}" class="pure-form">
                        <fieldset>
                            @csrf

                            <input type="text" class=" @error('search') is-invalid @enderror" name="search" value="" required placeholder="Search" autocomplete="search" >
                            <button type="submit" class="pure-button pure-button-primary">Search</button>

                            @error('search')
                            <span class="pure-form-message" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror



                        </fieldset>

                    </form>
                </div>
            </div>
        </div>

        <div class="container">

            @if($book)

                <div class="row mt-5 align-content-center">

                    <div class="col-5 col-sm-12">
                        @if( $book['authors'] && (strlen($book['authors'])<80) )
                            <h3>{{ $book['authors']}}<br>{{ $book['title']}}</h3>
                        @else
                            <h3>{{ $book['title']}}</h3>
                            <h4>{{ $book['authors']}}</h4>
                        @endif
                        @if($book['sequence_name'])<h5>{{ $book['sequence_name'] }}</h5>@endif

                        <a class="pure-button pure-button-primary" href="{{ route('download',[$book['archive_id'], $book['file_name']]) }}">Download fb2</a>
                        @if($book['annotation'])<hr><div>{!! $book['annotation'] !!}</div>@endif
                        <hr>
                        @if($book['lang'])     <div><label>Lang:</label> {{ $book['lang'] }}</div> @endif
                        @if($book['lang_src']) <div><label>Translate:</label> {{ $book['lang_src'] }}</div> @endif
                        @if($book['publish_info'])<hr>{!! $book['publish_info']  !!}@endif

                    </div>

                    <div class="col-5 col-sm-12 p-5">
                        <img alt="{{ $book['cover']['name']??"default-cover" }}" src="{{ ($book['cover']['data'] ?? '/imgs/default-book.jpg') }}" style="max-width: 100%">
                    </div>
                </div>






            @else

                <h2>Book not found (Error <b>404</b>)</h2>
                <p>Accept our regrets</p>

            @endif


        </div>

    </main>
@endsection
