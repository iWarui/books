<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->integer('archive_id')->unsigned();
            $table->string('file_name',100); //file_name
            $table->string('authors',3000);
            $table->string('title',2000); //title
            $table->string('date',500);
            $table->string('lang',200);
            $table->string('lang_src',2000); //lang_src
            $table->string('sequence_num',200);
            $table->string('sequence_name',2000); //sequence_name
            $table->mediumText('annotation');
            $table->string('genres',1000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
