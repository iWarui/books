<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['id' => 0, 'alias' => 'guest', 'title' => 'Guest'],
            ['id' => 1, 'alias' => 'user',  'title' => 'User'],
            ['id' => 2, 'alias' => 'admin', 'title' => 'Administrator'],
        ];

        foreach ($roles as $vals) {
            DB::table('roles')->insert($vals);
        }
    }
}
