<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=UserSeeder

        DB::table('users')->insert([
            'name'      => env('APP_ADMIN_NAME'),
            'email'     => env('APP_ADMIN_EMAIL'),
            'password'  => Hash::make(env('APP_ADMIN_PSWD')),
            'role'      => 2, //'admin'
        ]);
    }
}
