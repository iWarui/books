<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{

    public function archive()
    {
        return $this->hasOne('App\Archive','id','archive_id');
    }


    public function scopeFindAllInArchive($query, $archive) {
        $query->where('archiv_path', '=', $archive);
        return $query;
    }





}
