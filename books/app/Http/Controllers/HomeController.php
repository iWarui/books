<?php

namespace App\Http\Controllers;




use App\Http\Services\Library;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $page = [];
    private $lib;


    public function __construct()
    {
        $this->page = [
            'title' => 'Find this Book!'
        ];
        $this->lib = new Library();
    }

    public function index( Request $request )
    {
        $search = $request->search;
        return view('welcome', [
            'page'=>$this->page,
            'search'=>$search,
            'statistic'=>$this->lib->get_statistic()
        ]);
    }


    public function search( Request $request )
    {
        $results = $this->lib->search($request->search);
        return view('welcome', ['page'=>$this->page,'search'=>$request->search,'results'=>$results]);
    }


    public function book_page($archive_id,$fileName)
    {
        $book = $this->lib->get_book($archive_id,$fileName);
        return view('book', ['page'=>$this->page,'book'=>$book]);
    }

    public function download($archive_id,$fileName)
    {
        return $this->lib->download($archive_id,$fileName);
    }

}
