<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Services\AdminBooks;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    protected $page = [];
    protected $adminBooks;


    public function __construct(AdminBooks $adminBooks )
    {
        $this->adminBooks = $adminBooks;
        $this->page = [
            'title' => 'Administration',
        ];
    }


    public function index()
    {
        $this->page['view'] = 'admin_statistics';
        return view('admin', ['page'=>$this->page]);
    }



    public function update_books()
    {
        $this->page['view'] = 'admin_update_books';

        //$this->adminBooks->sync_archive('lib.rus.ec/fb2-000065-572310_lost.zip'); //readDisk();    someLib/1.fb2
        return view('admin', [
            'page'=>$this->page,
            'logs' => $this->adminBooks->logs,
        ]);
    }

    public function findAllFiles() // ajax
    {
        echo json_encode( ['status'=>'ok','archives'=>($this->adminBooks->get_archives()) ] ); // getFiles(true)
        //else echo json_encode(['status'=>'error','msg'=>'Request error. Wrong parameters ']);
    }

    public function clearAllBooks()
    {
        $this->adminBooks->db_clear();
        return json_encode( ['status'=>'success', 'msg' => 'Error clearing tables'] );
    }

    public function create_indexes()
    {
        $time = $this->adminBooks->db_add_search_indexes();
        return json_encode( ['status'=>'ok', 'msg' => 'Indexes created at '.gmdate("i:s", intval($time)), 'data'=>['time'=>$time]] );
    }

    public function parseOneArhive(Request $req) // ajax
    {
        if($req->file) {
            $result = $this->adminBooks->sync_archive($req->file); //['count'=>(int), 'errors'=> (int), 'time'=>(float)]
            echo json_encode( [
                'status'    => $result['fb2_read_errors']>0 ? 'error' : 'success',
                'data'      => $result,
                'msg'       => 'Time: ' . gmdate("H:i:s", intval($result['time']))] );
        } else echo json_encode( ['status'=>'error', 'msg' => 'Request error: parameters'] );
    }




    public function update_zip(Request $request)
    {
        $this->page['view'] = 'admin_update_zip';

        $result = $this->adminBooks->sync_archive( 'lib.rus.ec/fb2-166043-168102.zip' );

        $error = [];
        if($request && ($request->zip ?? false)) {
            $validated = $this->validate($request, [
                'zip'             => 'required|min:5|max:30' //fb2-999999-999999.zip
            ]);
            var_dump($validated);
        }
        return view('admin', ['page'=>$this->page]);
    }




    public function users()
    {
        $this->page['view'] = 'admin_users';
        return view('admin', ['page'=>$this->page]);
    }


}
