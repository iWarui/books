<?php


namespace App\Http\Controllers;





use App\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    protected $page = [];


    public function __construct( )
    {
        $this->page = [
            'title' => 'Profile'
        ];
    }


    public function index( )
    {
        return view('profile', [
            'page'=>$this->page,
        ]);
    }
}
