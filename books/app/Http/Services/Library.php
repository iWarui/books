<?php


namespace App\Http\Services;


use App\Archive;
use App\Books;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;
use ZipArchive;

class Library
{

    private $disk;


    public function __construct()
    {
        $this->disk = Storage::disk('books');
    }


    public function search($search_str)
    {
        //$fields = ['archive_id','file_name','title','authors','sequence_name','sequence_num','lang','lang_src','annotation','date'];
        $books = DB::select('
            SELECT *
            FROM books 
            WHERE make_tsvector(authors, title, sequence_name, lang_src ) @@ plainto_tsquery( ? )', [$search_str]);
        $authors = DB::select('
            SELECT authors, similarity(authors, ? ) AS rel
            FROM books
            WHERE authors % ?
            GROUP BY authors
            ORDER BY rel DESC, authors;', [$search_str, $search_str]);
        return ['authors'=>$authors, 'books'=>$books];
    }

    public function get_book($archive_id,$fileName)
    {
        //$fields = ['archive_id','file_name','title','authors','sequence_name','sequence_num','lang','lang_src','annotation','date'];
        $book = Books::where(['archive_id'=>$archive_id,'file_name'=>$fileName])->first();
        if($book) $book = $this->findDescription($book);
        return $book;
    }

    public function download($archive_id,$fileName)
    {
        $book = Books::where(['archive_id'=>$archive_id,'file_name'=>$fileName])->first();
        $name =
            (strlen($book->authors)>40?mb_substr($book->authors,0,40)."... ":$book->authors).
            ($book['title']!=''?" - ".$book['title']:"").
            ($book['sequence_name']!=''?" [".$book['sequence_name']."]":'').
            ( ($book['title']=='')&&($book['sequence_name']=='') ? $book->file_name : ".fb2");

        $z = new ZipArchive();
        $fullPathFile = $this->disk->getAdapter()->applyPathPrefix( $book->archive->archiv_path );
        if( $z->open($fullPathFile) ) {
            $fb2str = $z->getFromName($book->file_name);
            $this->disk->put('tmp/'.$book->file_name.'.fb2', $fb2str);
            $headers = [];
            $headers[] = 'Content-Type: text/xml';
            $headers[] = 'Content-Length: ' . $this->disk->size('tmp/'.$book->file_name.'.fb2');
            $headers[] = 'Content-Disposition: attachment; filename="' . $name . '.zip"';
            return $this->disk->download('tmp/'.$book->file_name.'.fb2', $name, $headers);
        }
        return false;
    }

    public function get_statistic()
    {
        $counts = []; // books, date_lastUpdate
        $counts['books'] = Books::count();
        $counts['date_lastUpdate'] = Archive::orderBy('created_at','desc')->first(); $counts['date_lastUpdate'] = $counts['date_lastUpdate']->created_at;

        return $counts;
    }






    private function getFB2Encode($fb2string)
    {
        $tmp = array();
        preg_match("/encoding=([\"\'])(.*?)([\"\'])/", $fb2string, $tmp);
        if (!isset($tmp[2])) $tmp[2] = 'UTF-8';
        return $tmp[2];
    }


    private function findDescription($book)
    {
        $fb2book = [];

        $fb2book['archive_id']      = $book->archive->id;
        $fb2book['file_name']       = $book->file_name;
        $fb2book['title']           = $book->title;
        $fb2book['authors']         = strlen($book->authors)>0 ? $book->authors : false;
        $fb2book['sequence_name']   = strlen($book->sequence_name)>0 ? $book->sequence_name : false;
        $fb2book['sequence_num']    = strlen($book->sequence_num)>0 ? $book->sequence_num : false;
        $fb2book['lang']            = strlen($book->lang)>0 ? $book->lang : false;
        $fb2book['lang_src']        = strlen($book->lang_src)>0 ? $book->lang_src : false;
        $fb2book['date']            = strlen($book->date)>0 ? $book->date : false;


        // read file
        $fullPathFile = $this->disk->getAdapter()->applyPathPrefix( $book->archive->archiv_path );
        $z = new ZipArchive();
        libxml_use_internal_errors(true);
        if( $z->open($fullPathFile) ) {
            $fb2str = $z->getFromName($book->file_name);
            $fb2xml = simplexml_load_string($fb2str);
            if ($fb2xml === false) echo "error 2 (load xml)"; // error read file data
            else {
                $fb2xml->registerXPathNamespace('fb2', 'http://www.gribuser.ru/xml/fictionbook/2.0');
                $fb2xml->registerXPathNamespace('l', 'http://www.w3.org/1999/xlink');

                $annotation = $fb2xml->xpath('//fb2:annotation');
                if ($annotation[0] ?? false) $fb2book['annotation'] = $annotation[0]->asXML();
                else $fb2book['annotation'] = false;

                $cover_d = $fb2xml->xpath('//fb2:coverpage/fb2:image');
                if( !$cover_d ) {
                    $cover_d = $fb2xml->xpath('//fb2:image');
                } // //fb2:coverpage/fb2:image

                if( $cover_d ) {
                    $fb2book['cover']['name'] = $cover_d[0]->attributes('l', true) ?? '';

                    if ($fb2book['cover']['name'] != '') {
                        $fb2book['cover']['name'] = substr($fb2book['cover']['name'], 1);
                        $binary = $fb2xml->xpath('//fb2:binary'); // binary
                        foreach ($binary as $bp) {
                            if ((string)$bp->attributes()['id'] == $fb2book['cover']['name']) {
                                $cover_type = $bp->attributes()['content-type'];
                                $fb2book['cover']['data'] = 'data:' . $cover_type . ';base64,' . $bp[0];
                            }
                        }
                    }
                }

                /*
                if($cover_d[0]??false) {
                    $cover_name = $cover_d[0]->attributes('l', TRUE)->href;
                } else {
                    $cover_d = $fb2xml->xpath('//fb2:image');
                    if($cover_d[0]??false) $cover_name = $cover_d[0]->attributes('l', TRUE)->href;
                }
                */




                $publish_info = $fb2xml->xpath('//fb2:publish-info');
                if ($publish_info[0] ?? false) {
                    $tags_base = ['publish-info','book-name','publisher','city','year','isbn'];
                    $tags_repl = ['div','div','div','div','div','div'];
                    $fb2book['publish_info'] = str_replace($tags_base, $tags_repl,$publish_info[0]->asXML());
                }
                else $fb2book['publish_info'] = false;

                //dd($fb2book);
                return $fb2book;
            }

        }

        return false;
    }






    private function utf16_to_utf8($str) {
        $c0 = ord($str[0]);
        $c1 = ord($str[1]);

        if ($c0 == 0xFE && $c1 == 0xFF) {
            $be = true;
        } else if ($c0 == 0xFF && $c1 == 0xFE) {
            $be = false;
        } else {
            return $str;
        }

        $str = substr($str, 2);
        $len = strlen($str);
        $dec = '';
        for ($i = 0; $i < $len; $i += 2) {
            $c = ($be) ? ord($str[$i]) << 8 | ord($str[$i + 1]) :
                ord($str[$i + 1]) << 8 | ord($str[$i]);
            if ($c >= 0x0001 && $c <= 0x007F) {
                $dec .= chr($c);
            } else if ($c > 0x07FF) {
                $dec .= chr(0xE0 | (($c >> 12) & 0x0F));
                $dec .= chr(0x80 | (($c >>  6) & 0x3F));
                $dec .= chr(0x80 | (($c >>  0) & 0x3F));
            } else {
                $dec .= chr(0xC0 | (($c >>  6) & 0x1F));
                $dec .= chr(0x80 | (($c >>  0) & 0x3F));
            }
        }
        return $dec;
    }
}
