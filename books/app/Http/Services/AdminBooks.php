<?php


namespace App\Http\Services;


use App\Archive;
use App\Books;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use ZipArchive;
use SimpleXMLElement;

class AdminBooks
{

    private $log_only_error = true;

    protected $z; // ZipArchive()
    protected $disk,$diskLog; // Storage::disk()

    // reading fb2
    private $charChunk      = 1000;
    private $charLimit      = 100000; // limit chars read from fb2-file
    private $fb2string      = '';
    private $namespaces     = false;

    public $logs = []; // ['status'=>(info||error||debug), 'msg'=>(string)]

    private $book_fields = array(
        'authors',
        'title',
        'date',
        'lang',
        'lang_src',
        'sequence_num',
        'sequence_name',
        'annotation',
        'archive_id',
        'file_name',
        'genres'
    );
    private $books = [];
    private $books_dbLimit = 100;
    private $archives;
    private $current_archive = false;
    private $current_archive_id = false;


    public function __construct()
    {
        $this->z = new ZipArchive();
        $this->disk = Storage::disk('books');
        $this->diskLog = Storage::disk('logs');
    }







    // Work with DB


    private function db_save_books()
    {
        Books::insert($this->books);
        $this->books = [];
        return true;
    }

    private function db_clear_archive_books($id=false)
    {
        $count = Books::where('archive_id',($id ? $id : $this->current_archive->id) )->count();
        Books::where('archive_id',($id ? $id : $this->current_archive->id) )->delete();
        return $count;
    }

    private function db_clear_books_by_list_fb2($archive_id, $list )
    {
        Books::where('archive_id',$archive_id)->whereIn('file_name', $list)->delete();
    }

    private function db_delete_archive($id)
    {
        Archive::where('id', $id)->delete();
    }

    private function db_add_new_archive($file_path)
    {
        $arc = new Archive;
        $arc->archiv_path = $file_path;
        $arc->save();
        return $arc;
    }

    private function db_get_books_by_archive($id, $select_fields=['file_name'])
    {
        return Books::select($select_fields)->where('archive_id',$id)->get();
    }

    public function db_clear()
    {
        Archive::truncate();
        Books::truncate();
        DB::statement("DROP INDEX IF EXISTS idx_fts_book; ");
        DB::statement("DROP INDEX IF EXISTS authors_trgm_idx; ");
    }

    public function db_books_count()
    {
        return Books::count();
    }

    private function db_get_archive_id_by_name( $archive_path )
    {
        $id = false;
        foreach ($this->archives as $archive) {
            if($archive->archiv_path == $archive_path) {
                $id =  $archive->id;
            }
        }
        return $id;
    }

    private function db_find_archives()
    {
        $this->archives = Archive::all();
    }

    public function db_add_search_indexes(){
        set_time_limit (300);
        $microtime = microtime(true);
        DB::statement("CREATE EXTENSION IF NOT EXISTS pg_trgm;");

        DB::statement("CREATE OR REPLACE FUNCTION make_tsvector(authors TEXT, title TEXT, sequence_name TEXT, lang_src TEXT)
           RETURNS tsvector AS $$
        BEGIN
          RETURN (setweight(to_tsvector('russian', authors),'A') ||
                            setweight(to_tsvector('russian', title),'C') ||
                  setweight(to_tsvector('russian', sequence_name), 'B') ||
                            setweight(to_tsvector('russian', lang_src),'D') );
        END
        $$ LANGUAGE 'plpgsql' IMMUTABLE; ");
        DB::statement("CREATE INDEX IF NOT EXISTS idx_fts_book ON books
          USING gin(make_tsvector(authors, title, sequence_name, lang_src));");
        sleep(1);
        DB::statement("CREATE INDEX IF NOT EXISTS authors_trgm_idx ON books USING GIST (authors gist_trgm_ops);");
        return round(microtime(true) - $microtime );
    }





    // Work with files


    public function download_fb2($archive_id,$filename)
    {
        $name = ' ';
        $headers = '';
        return $this->disk->download('file.jpg', $name, $headers);
    }

    private function log($log_name,$action,$status,$msg='')
    {
        if($this->log_only_error && ($status!='error')) return false;
        // $data = [ archive_name, action, status, msg ]
        $logName = basename($log_name.'.log');
        if(!$this->diskLog->exists($logName)) {
            $this->diskLog->put($logName, 'Log file created: '.date('Y-m-d H:i:s'));
        }

        $content = date('Y-m-d H:i:s') . ' [' . $action . '] [' . $status . '] ' . $msg;// . PHP_EOL;
        $this->diskLog->append($logName,$content);
    }






    public function get_archives()
    {
        $this->db_find_archives();
        $list = [];

        $c_in_db = 0;
        $c_in_disk = 0;
        $c_overlap = 0;

        foreach ($this->archives as $arc){
            $fullPathFile = $this->disk->getAdapter()->applyPathPrefix($arc->archiv_path);
            $path_info = pathinfo($fullPathFile);
            $ext = $path_info['extension'];
            $list[ $arc->archiv_path ] = [
                'name'      => $arc->archiv_path,
                'file'      => false,
                'fullpath'  => $fullPathFile,
                'ext'       => $ext,
                'bd_data'   => ['id'=>$arc->id,'created_at'    => $arc->created_at]
            ];
        }
        $c_in_db = count($list);

        $dirs = $this->disk->directories();
        if($dirs){
            foreach ($dirs as $dir) {
                $files = $this->disk->files($dir);
                foreach ($files as $file){
                    $fullPathFile = $this->disk->getAdapter()->applyPathPrefix($file);
                    $path_info = pathinfo($fullPathFile);
                    $ext = $path_info['extension'];
                    if(in_array($ext, ['zip'])) { // ,'fb2'

                        if( ($list[ $file ] ?? false) ) {
                            $list[ $file ]['file'] = true;
                            $c_overlap++;
                        } else {
                            $list[ $file ] = [
                                'name'      => $file,
                                'file'      => true,
                                'fullpath'  => $fullPathFile,
                                'ext'       => $ext,
                                'bd_data'   => false
                            ];
                        }
                        $c_in_disk++;
                    }
                }
            }
        }
        //krsort($list);
        $this->log('list_of_file','get_list','ok','Count of files:'.count($list) . ' DB:'.$c_in_db.' Disk:'.$c_in_disk.' Overlap:'.$c_overlap);
        return array_values($list);
    }

    public function sync_archive( $fileName )
    {
        set_time_limit (3600);
        $microtime = microtime(true);
        usleep(50000);


        $this->log($fileName,'synchronize','start','');

        $archives_on_disk = $this->get_archives();
        $readedFB2 = 0;
        $errors = 0;
        $filesfb2list = [];
        $list_to_updated = []; // list of fb2 files to read and save to db
        $list_to_delete  = [];  // list of fb2 files to delete from db
        $archive_to_delete = false; // id archive for deleting from db

        $found_archive = false;
        foreach ($archives_on_disk as $file) {
            if ($fileName === $file['name']) {
                if( $file['file'] ) $found_archive = $file;
                break;
            }
        }

        if($found_archive){

            $this->log($file['name'],'synchronize','found','Archive ' . $file['name'] . ' on disk');

            $filesfb2list = $this->find_all_fb2( $found_archive );
            $tmp = [];
            foreach ($filesfb2list as $fb2) { $tmp[$fb2['fb2Name']] = $fb2; }
            $filesfb2list = $tmp;

            $archive_id = $found_archive['bd_data']['id'] ?? false;

            if($archive_id) {
                // archive already in db, check all fb2
                $this->current_archive_id = $archive_id;
                $dbfb2list = $this->db_get_books_by_archive($archive_id);

                $tmp = [];
                foreach ($dbfb2list as $fb2) { $tmp[$fb2->file_name] = $fb2->file_name; }
                $dbfb2list = $tmp;

                $list_to_updated = array_diff_key($filesfb2list, $dbfb2list);
                $list_to_delete  = array_diff_key($dbfb2list, $filesfb2list);
                $this->log($file['name'],'synchronize','status','Count to update: ' . count($list_to_updated) . ' to delete: '.count($list_to_delete));

            } else {
                // archive not found in db, read all fb2 files
                $this->current_archive = $this->db_add_new_archive($fileName);
                $this->current_archive_id = $this->current_archive->id;
                $list_to_updated = $filesfb2list;
            }

        } else {
            $this->log($file['name'],'synchronize','not found','Archive ' . $file['name'] . ' not exist on disk');
            $archive_to_delete = $this->db_get_archive_id_by_name( $fileName );
        }


        // process lists

        if(count($list_to_updated)>0) {
            // process reading fb2 files from arhives
            $this->log($file['name'],'synchronize','start','Count to update: '.count($list_to_updated));
            foreach ($list_to_updated as $fb2 ) {
                if( !$this->readFB2( $fb2 ) ) $errors++;
                $readedFB2++;
                if( count($this->books) >= $this->books_dbLimit ) {
                    $this->db_save_books();
                }
                //echo '<pre>'; var_dump($fb2); echo '</pre>';
            }
            $this->db_save_books();
            $this->log($file['name'],'synchronize','ok','Count readed fb2: '.$readedFB2 .' Errors: '.$errors);
        }

        if(count($list_to_delete)>0){
            $this->log($file['name'],'synchronize','start','Clear fb2 from DB: '.count($list_to_delete));
            $this->db_clear_books_by_list_fb2($archive_id, $list_to_delete );
        }

        if($archive_to_delete) {
            // process deleting all fb2-records from table:books & delete archive from table:archives
            $this->log($file['name'],'synchronize','start','Clear archive from DB: '.$archive_to_delete);
            $this->db_clear_archive_books( $archive_to_delete );
            $this->db_delete_archive( $archive_to_delete );
        }

        $time = microtime(true)-$microtime;
        $out = [
            'time'              => round($time),
            'fb2_readed'        => $readedFB2,
            'fb2_read_errors'   => $errors,
            'fb2_deleted'       => count($list_to_delete),
            'archive_is_deleted'=> ( $archive_to_delete ? true : false ),
            'archive_count'     => count($filesfb2list),
            'all_count'         => $this->db_books_count()
        ];
        $this->log($file['name'],'synchronize','ok','Output: '.json_encode($out));
        return $out;
    }




    public function find_all_fb2($file)
    {
        $list = [];

        if (($file['ext']=='zip') && $this->z->open( $file['fullpath'] )) {

            for ($i = 0; $i < $this->z->numFiles; $i++) {
                $fb2 = $this->z->statIndex($i);
                $fb2 = basename($fb2['name']);
                $fb2string = $this->z->getFromName($fb2, 100);
                $enc = $this->getFB2Encode($fb2string);

                $list[] = [
                    'id'        => $i,
                    'zip'       => $file['fullpath'],
                    'path'      => $file['name'],
                    'fb2Name'   => $fb2,
                    'encoding'  => strtoupper($enc)
                ];
            }

        } else {

            $fp = fopen($file['fullpath'], 'r');
            $fb2string = fread($fp, 100);
            $enc = $this->getFB2Encode($fb2string);

            $list[] = [
                'id'        => $i,
                'zip'       => false,
                'fb2Name'   => $file['fullpath'],
                'path'      => $file['name'],
                'encoding'  => strtoupper($enc)
            ];
        }
        $this->log($file['name'],'find_all_fb2','ok','Count of FB2: '.count($list));
        return $list;
    } // find_all_fb2()



    private function findDescription($fb2file)
    {

        $pattern = '#<description(?:\s+[^>]+)?>(.*?)</description>#s';
        $found = preg_match_all($pattern, $this->fb2string, $matches, PREG_PATTERN_ORDER);
        if($matches[0][0] ?? false){


            $pattern_names = '#<FictionBook(?:\s+[^>]+)?>#s';
            $found = preg_match_all($pattern_names, $this->fb2string, $names, PREG_PATTERN_ORDER);
            $this->namespaces = $names[0][0] ?? false;

            if($this->namespaces && false) {
                echo '<pre style="margin: 1em; border: 1px solid gray;">';
                echo htmlentities($names[0][0] ?? 'false');
                echo '</pre>';
            }

            if($fb2file['zip'] ?? false) {
                $fileName = $fb2file['fb2Name'];
                $path = $fb2file['path'];
            }
            else {
                $fileName = basename($fb2file['fb2Name']);
                $path = $fb2file['path'];
            }

            $this->fb2string = $matches[0][0];
            $book = $this->prepare_fb2_data(  $path, $fileName );

            if(!$book) return false;

            // save book in insert_array
            $this->books[] = $book;

            return true;
        } else {
            //$this->log( $fb2file['path'],'findDescription','warning','Tag `description` not found');
        }
        return false;
    }



    private function readFB2( $fb2file )
    {
        usleep(5000);
        //$this->logs[] = ['status'=>'debug', 'msg' => 'Read '. implode(' | ',$fb2file)];

        $this->fb2string = '';

        if($fb2file['zip']) { // если файл fb2 в архиве

            $fp = fopen('zip://' . $fb2file['zip'] . '#' . $fb2file['fb2Name'], 'r');

        } else {
            $fp = fopen( $fb2file['fb2Name'], 'r');
        }

        if($fp) {
            $foundDescription = false;
            $readedChars = 0;
            while( !feof($fp) && ($readedChars < $this->charLimit) ) {

                $chunk = fread($fp, $this->charChunk);
                $readedChars =  $readedChars + $this->charChunk;
                try {

                    if ($fb2file['encoding'] == 'UTF-16') {
                        $this->log( $fb2file['path'],'readFB2','convert',($fb2file['id']??'0') . ' File '.$fb2file['fb2Name']. ' in ' .$fb2file['encoding']);
                        $chunk = $this->utf16_to_utf8($chunk);
                    }
                    else if($fb2file['encoding'] != 'UTF-8') {
                        $this->log( $fb2file['path'],'readFB2','convert',($fb2file['id']??'0') . ' File '.$fb2file['fb2Name']. ' in ' .$fb2file['encoding']);
                        $chunk = mb_convert_encoding($chunk, 'UTF-8', $fb2file['encoding']);
                    } else {
                        $this->log( $fb2file['path'],'readFB2','ok',($fb2file['id']??'0') . ' File '.$fb2file['fb2Name']. ' in ' .$fb2file['encoding']);
                    }
                } catch (Exception $e) {
                    $this->log( $fb2file['path'],'readFB2','error',($fb2file['id']??'0') . ' File '.$fb2file['fb2Name']. ' can`t convert from ' .$fb2file['encoding']);
                    break;
                }
                $this->fb2string .= $chunk;

                if($this->findDescription($fb2file)) {
                    $foundDescription = true;
                    break;
                }
            }

            if($foundDescription)  return true;
            else {
                $this->log( $fb2file['path'],'readFB2','warning','Tag `description` not found');
            }

        } else {
            $this->log( $fb2file['path'],'readFB2','error','Can`t open '.$fb2file['fb2Name']);
        }

        return false;
    }




    private function prepare_fb2_data($path,$file_name)
    {
        //return false;

        $xmlstring = '<?xml version="1.0" encoding="utf-8"?>'.($this->namespaces ?? '<FictionBook xmlns="http://www.gribuser.ru/xml/fictionbook/2.0" xmlns:xlink="http://www.w3.org/1999/xlink">').$this->fb2string.'</FictionBook>';

        try {
            $fb2xml = new SimpleXMLElement($xmlstring);
        }catch (Exception $exception) {
            $this->log( $path,'prepare_fb2_data','error','File '.$file_name. ' can`t convert to XML' );
            return false;
        }
        if($fb2xml === false)
        {
            return false;
        }
        else // if( xml ok )
        {
            $book = [];
            foreach ($this->book_fields as $book_field) {
                $book[$book_field] = '';
            }

            //echo '<pre>'; var_dump($fb2xml);

            $fb2xml = $fb2xml->{'description'};
            $fb2xml = $fb2xml->{'title-info'};


            $book['archive_id'] = $this->current_archive_id;
            $book['file_name'] = basename($file_name); // fb2

            $book['title']	= (string)$fb2xml->{'book-title'};
            $book['lang']	= (string)( $fb2xml->{'lang'} ?? '' );
            $book['date']	= (string)( $fb2xml->{'date'} ?? '' );
            $book['lang_src']	= (string)( $fb2xml->{'src-lang'} ?? '' );
            if($fb2xml->{'translator'}) {
                $tr = '';
                foreach ($fb2xml->{'translator'} as $translator) {
                    $tr  .= ($tr==''?"":", ").$translator->{'first-name'}." ".$translator->{'middle-name'}." ".$translator->{'last-name'};
                }
                $book['lang_src']	.= ($book['lang_src']!=''?";":"").$tr;
            }

            $sequence = $fb2xml->{'sequence'};
            $book['sequence_name']	= (string)( $sequence['name'] ?? '' );
            $book['sequence_num']	= (string)( $fb2xml->{'sequence'}['number'] ?? '' );
            if(count($sequence)>0){

                foreach ($sequence->{'sequence'} as $s) {
                    $book['sequence_name'] .= ($book['sequence_name']!='' ? ($s['name']!=''?"[".$s['name']."]":"") : ($s['name']!=''?$s['name']:""));
                    $book['sequence_num'] .= ($book['sequence_num']!='' ? ($s['number']!=''?"-".$s['number']:'') : ($s['number']!=''?$s['number']:''));
                }
            }

            /*
            if($fb2xml->{'annotation'})
            {
                $annotation = $fb2xml->{'annotation'}->asXML();
                $book['annotation']	=  ($annotation);
            }
            */

            $authors = $fb2xml->author??false;
            if($authors) {

                if(is_object($authors)) {
                    $first  = $authors->{'first-name'}  ?? false;
                    $middle = $authors->{'middle-name'} ?? false;
                    $last   = $authors->{'last-name'}   ?? false;
                    $book['authors'] = trim($last ?? '') . ($first ? ' '.trim($first) : '') . ($middle ? ' '.trim($middle) : '');
                } elseif(is_array($authors)) foreach ($authors as $a) {
                    $first  = $a->{'first-name'}  ?? false;
                    $middle = $a->{'middle-name'} ?? false;
                    $last   = $a->{'last-name'}   ?? false;
                    $book['authors'] = ($book['authors']!='' ? '; ' : '') . trim($last ?? '') . ($first ? ' '.trim($first) : '') . ($middle ? ' '.trim($middle) : '');
                } else $book['authors'] = ' - ';
            }

            $genres = $fb2xml->genre??false;

            if($genres ) {

                foreach ($genres as $genre) {
                    $book['genres'] .= ($book['genres']!='' ? ";" : "") . trim($genre);
                }
            }

            //echo '<pre>'; var_dump($book); echo '</pre>';
            foreach ($book as $f=>$v) {
                if(is_string($v)) $book[$f] = trim($v);
            }
            return $book;
        } // if( xml ok )


    } // prepare_fb2_data();




    private function getFB2Encode($fb2string)
    {
        $tmp = array();
        preg_match("/encoding=([\"\'])(.*?)([\"\'])/", $fb2string, $tmp);
        if (!isset($tmp[2])) $tmp[2] = 'UTF-8';
        return $tmp[2];
    }

    private function utf16_to_utf8($str) {
        $c0 = ord($str[0]);
        $c1 = ord($str[1]);

        if ($c0 == 0xFE && $c1 == 0xFF) {
            $be = true;
        } else if ($c0 == 0xFF && $c1 == 0xFE) {
            $be = false;
        } else {
            return $str;
        }

        $str = substr($str, 2);
        $len = strlen($str);
        $dec = '';
        for ($i = 0; $i < $len; $i += 2) {
            $c = ($be) ? ord($str[$i]) << 8 | ord($str[$i + 1]) :
                ord($str[$i + 1]) << 8 | ord($str[$i]);
            if ($c >= 0x0001 && $c <= 0x007F) {
                $dec .= chr($c);
            } else if ($c > 0x07FF) {
                $dec .= chr(0xE0 | (($c >> 12) & 0x0F));
                $dec .= chr(0x80 | (($c >>  6) & 0x3F));
                $dec .= chr(0x80 | (($c >>  0) & 0x3F));
            } else {
                $dec .= chr(0xC0 | (($c >>  6) & 0x1F));
                $dec .= chr(0x80 | (($c >>  0) & 0x3F));
            }
        }
        return $dec;
    }


}
